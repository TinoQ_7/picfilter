const winston = require('winston');
winston.emitErrs = true;
const config = require ('config');
const path = require('path');

let  logLoc = config.get('log.loc');
let datetime = new Date().toISOString().replace(/-|:|\..+/g, '').split('T')[0];
let fileName = datetime+'.log';
fileName = path.join(logLoc, fileName);

var logger = new winston.Logger({
    transports: [

        new winston.transports.File({
            name: 'logFile',
            level: 'debug',
            filename: fileName,
            handleExceptions: true,
            json: false,
            maxsize: 5242880, //5MB
//            maxFiles: 5,
            colorize: false
        }),
/*
        new winston.transports.Console({
            name: 'console'
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: false,
            timestamp: true
        })
*/
    ],
    exitOnError: false
});

let turnOff = config.get('log.turnOff');
logger.transports['logFile'].silent = turnOff;  //true means no file log, false means log

logger.info("Logger initialized!");

module.exports = logger;