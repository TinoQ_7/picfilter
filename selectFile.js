var config=require('config');
var async=require("async");
var parser=require('./fileNameParser');
var filter=require('./filters');
var util = require('util');
var dbgLog = require('debug')('selectFile');
const logger = require ('./logger');

exports.selectFiles = function(zipDict, callback) {
    let allFiles=[];

    let selectedDict={}    //a dictionary with zipName as key, and list of selected files from that zip file
    
    async.waterfall ([
        async.apply(getFileInfoFromFileName, zipDict),
        filterFiles
    ], function(err, selectedDict){
        if (err) return callback(err);
        callback(null, selectedDict);
    });
}


function getFileInfoFromFileName (zipDict, callback) {
    let faceSetDict={}     //a dictionary with fid as key, and list of file fields object for that fid
    let filterCond=config.get('filterCond');

    //async.eachLimit(Object.keys(zipDict), 2, function(zipName, callback){
    Object.keys(zipDict).forEach(function(zipName) {
        let fileList=zipDict[zipName];
        fileList.forEach(function(fileName){
            //parse the file name to get all file properties
            let fields=parser.parseFilename(fileName, filterCond, true);
            if (null != fields && null != fields.ref) {
                let fid = fields.fid;
                if (!(fid in faceSetDict)) {
                    faceSetDict[fid]=[];
                }
                fields.zip=zipName;
                faceSetDict[fid].push(fields);
            }
        });
    });
    callback(null, faceSetDict);
}

function filterFiles (faceSetDict, callback) {
    let filterCond=config.get("filterCond");
    let useFilter=filterCond.useFilter;
    let n=filterCond.maxPick;
    let minPic=filterCond.minPicNumForConfFilter;
    let selectedList=[];
    let selectedDict={}    //a dictionary with zipName as key, and list of selected files from that zip file

    Object.keys(faceSetDict).forEach(function(fid) {
        let fileFieldList=[];
        let pickedList=[];
        
        fileFieldList = filter.filterByConf(faceSetDict[fid], minPic);
        switch (useFilter){
            case 0:
                pickedList=filter.filterByIndex(fileFieldList, n);
                break;
            case 1:
                pickedList=filter.findBestPicByYValue(fileFieldList, n);
                break;
            default:
                let err="Selected filter "+useFilter+" is not defined";
                util.log(err);
                return (err);
        }

        async.eachOfLimit(pickedList, 1, function(fields, index, callback){
        //    util.log('Working on pickedList '+JSON.stringify(pickedList));
            if (null == fields) {
                logger.debug('Null on pickedList '+JSON.stringify(pickedList));
            } else {
                let zipName=fields.zip;
                if (!(zipName in selectedDict)){
                    selectedDict[zipName]=[];
                }
                selectedDict[zipName].push(fields.ref);
            }
            callback(null, null);
        });
    });
    callback (null, selectedDict);
}
