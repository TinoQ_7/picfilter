var util = require('util');
var extend = require('util')._extend;
var path = require('path');
var dbgLog = require('debug')('filters');
var config = require('config');
var parser = require('./fileNameParser');
const logger = require ('./logger');

var compLocalConf = function (a ,b){
    return (b.localConf - a.localConf);
};

var ascendAngleP = function (a ,b){
    return (Math.abs(a.angle_p) - Math.abs(b.angle_p));
};

let yOffset = 0;

var ascendAbsAngleY = function (a ,b){
    return (Math.abs(a.angle_y - yOffset) - Math.abs(b.angle_y - yOffset));
};

var ascendAngleY = function (a ,b){
    return (a.angle_y - b.angle_y);
};


var fifoOrder = function (a, b) {
    if (a.tstamp < b.tstamp) {
        return -1;
    }
    else if (a.tstamp > b.tstamp) {
        return 1;
    }
    else if (a.fid != b.fid){
        return (a.fid - b.fid);
    }
    else {
        return (a.sn - b.sn);
    }
};

var extractRef = function(fset) {
    let refList = [];
    fset.sort(fifoOrder)
    fset.forEach(function(f){
        refList.push(f.ref);
    });
    return refList;
};

exports.filterByConf = function(faceSet, n) {
//    logger.debug('filterByConf() input faceSet is ', JSON.stringify(faceSet), faceSet.length);
    let r=[];

    if (n<=0 || faceSet.length<n){
        r=faceSet;
        return r;
    }

    faceSet.sort(compLocalConf);

    let nthConf = faceSet[n-1].localConf;
//    logger.debug('nthConf:', nthConf);
    r = faceSet.filter(face => face.localConf >= nthConf);

    return r;    
};

exports.filterByIndex = function (faceSet, n) {
    logger.debug('Input faceSet is ', JSON.stringify(faceSet));
    if (n<=0){
        return [];
    }
    let r=[];
    if (faceSet.length<n){
        r=faceSet;
        return r;
    }

    faceSet.sort(fifoOrder);
    if (1==n){
        let pick=Math.round(faceSet.length/2);
        r.push(faceSet[pick]);
    } else  {
        //first select the both extrem index
        r.push(faceSet[0]);
        r.push(faceSet[(faceSet.length-1)]);
        n-=2;
        let lastPick=0;
        while (n>0) {
            let ref=Math.floor((faceSet.length-lastPick)/(n+1));
            lastPick+=ref;
            if (lastPick<faceSet.length-1){
                r.push(faceSet[lastPick]);
                n--;
            } else {
                break;
            }
        }
    }
    return r;
}


//ordered result list: [0]: minY in [min_P, min_p+5), [1]: max(abs(bestY-Y)) in [min_P, min_P+10), [3, 4]: uniPick over the reset of [min_P, min_P+10)
exports.findBestPicByYValue = function (faceSet, n) {
    if (0 < faceSet.length)
        logger.debug('Working on fid: '+faceSet[0].fid);
    logger.debug('Input faceSet is ', JSON.stringify(faceSet));
    if (n<=0){
        return [];
    }


    let cond=extend({}, config.get("filterCond"));
    let scaleChangeTime=new Date(cond.angleScaleChangeDate).getTime();
    let fileDate=faceSet[0].tstamp.split("T")[0];
    let fileTime=new Date(fileDate).getTime();
    if (fileTime >= scaleChangeTime) {
        cond.angleBW*=10;
    }

    let r=[];    

    if (faceSet.length<n){
        r=faceSet;
    } else {
        //seperate the list by P value, find small bin and large bin
        faceSet.sort(ascendAngleP);
        //console.log("order by P Value: ", JSON.stringify(faceSet));
        let s1 = [], s2 = [];
        let pMin = Math.abs(faceSet[0].angle_p);
        faceSet.forEach(function(fi){
            let ap = fi.angle_p;
            if ( ap < pMin + cond.angleBW){
                s1.push(fi);
            }
            else if (ap < (pMin + 2*cond.angleBW) ){
                s2.push(fi);
            }
        });
        //find best Y
        s1.sort(ascendAbsAngleY);
        let bestY=s1[0].angle_y;
        r.push(s1[0]);
        s1.shift();
        if (1 == n) {
            return r;
        }
        n--;


        //larger bin:
        s1=s1.concat(s2);
        s1.sort(ascendAngleY);
        //console.log("order by Y Value: ", JSON.stringify(s1));
        //find rest of pic

        if (s1.length<=n){
            r=r.concat(s1);
            return r;
        }

        let minDiffList=Array(s1.length).fill(99999);
        let lastPickInd=-1;
        while (n>0) { 

                //another method: 
                //same idea of finding mazimize the minimum distance to other points in the result set
                //use an array to store the min distance to any existing points in result set, 
                //each time just calculate the distance of all values in faceSet to the newly selected point
                //and update the min distance array and update the max value and point at the same time
                //let minDiffList=[];   
                /* this list stores the minimum distance of faceSet value to all points in result set
                   for example, minDiffList[i]=min(Math.abs(faceSet[i]-r[0,m])) r[0,m] means all values in the result set
                */
            let maxMinDiff=0;
            let lastPickV=0;
            if (-1==lastPickInd){
                //compare with bestY
                lastPickV=bestY;
            } else {
                lastPickV=s1[lastPickInd].angle_y;
            }

            let canInd=-1;
            for (let i=0; i<s1.length; i++){
                let diff = Math.abs(s1[i].angle_y-lastPickV);
                if (diff<minDiffList[i]){
                    minDiffList[i]=diff;
                }
                if (minDiffList[i] > maxMinDiff) {
                    maxMinDiff=minDiffList[i];
                    canInd=i;
                }
            }
            if (canInd>=0) {
                lastPickInd=canInd;
                r.push(s1[lastPickInd]);
            }
            n--;                     
        }
    }
    logger.debug('Picked '+r.length+' pictures, they are ', JSON.stringify(r));
    return r;
}


/*
    Purpose:
        uniformly pick n values from the sorted array
    Input:
        arr: array to be picked
        n:   number to pick
    Output:
        pList: array of picked index, return index here so the caller can use it as a utility function
               for example, caller can provide the index of an array as input, then it is uniPickByIndex
                            caller can provide the array Y values, then it is uniPickByYValue
                            caller can provide the array of sn, then it is uniPickBySn 
 */
var uniPickByValue= function (arr, n) {
    dbgLog('Input arr to uniPickByValue is ', JSON.stringify(arr), ' pick ', n);
    let pList=[];    //result set is the picked index of the input array
    let refValue=0;  //for uniformaly distributed array, refValue = the distance between each picked pair  
    let pIndex=-1;   //picked index
    let lRef=-1;    //the reference value from the left side
    let rRef=-1;   //the reference value from the right side
    let lPointer=0
    let rPointer=arr.length-1;   //min picked index from the right side 
    let pValue=new Set();

/*    if (n < 2||arr.length<3) {
        return [];   //do not use this function if pick number is less than 2
    } else {
        pList.push(0);
        pValue.add(arr[0]);
        pList.push(arr.length-1);
        pValue.add(arr[arr.length-1]);
        n-=2;
    }
*/
    while (n >0){
        refValue=(arr[rPointer]-arr[lPointer])/(n+1);    //pick n index means seperate array into n+1 sub parts
        lRef=arr[lPointer]+refValue;
        rRef=arr[rPointer]-refValue;
        let lDiff=-1;         //the difference between value of left pointer and the left reference value
        let lDiffPlus=-1;     //the difference between value of left pointer+1 and the left reference value
        let rDiff=-1;         //the difference between value of right pointer and the right reference value
        let rDiffPlus=-1;     //the difference between value of right pointer+1 and the right reference value
        let lFound=false;         //if true, found the pick index on the left side
        let rFound=false;         //if true, found the pick index on the right side
        for (let i=1; i<rPointer-lPointer; i++){
    //    while (lPointer<rPointer) {

            lDiff=Math.abs(lRef-arr[lPointer+i]);
            lDiffPlus=Math.abs(lRef-arr[lPointer+i+1]);
            if (lDiff < lDiffPlus) {
                //value closest to the left reference found
                lFound=true;
            }

            rDiff=Math.abs(arr[rPointer-i]-rRef);
            rDiffPlus=Math.abs(arr[rPointer-i-1]-rRef);
            if (rDiff < rDiffPlus) {
                //value closest to the right reference found
                rFound=true;
            }

            if (lFound && rFound) {  //if both side find the closest value at the same time, pick the one closer to the reference
            /*    if (n>=2){
                    if (!pValue.has(arr[lPointer+i])){
                        lPointer+=i;
                        pList.push(lPointer);
                        n--;
                    }
                    if (!pValue.has(arr[rPointer-i])){
                        rPointer-=i;
                        pList.push(rPointer);
                        n--;
                    }
                    //TODO: break if both done, otherwise, wait for next round to break
                } else {*/
                    if (lDiff <= rDiff && !pValue.has(arr[lPointer+i])) {
                        lPointer+=i;
                        pList.push(lPointer);
                        n--;
                        break;
                    } else {
                        if (!pValue.has(arr[rPointer-1])){
                            rPointer-=i;
                            pList.push(rPointer);
                            n--;
                            break;
                        }
                    }
            //    }
            } else if (lFound){
                if (!pValue.has(arr[lPointer+i])){
                    lPointer+=i;
                    pList.push(lPointer);
                    n--;
                    break;
                }
            } else if (rFound){
                if (!pValue.has(arr[rPointer-i])){
                    rPointer-=i;
                    pList.push(rPointer);
                    n--;
                    break;
                }
            }
        }
        if (!lFound && !rFound){
            //may hit this condition when the last few values were the same and equal to the lRef and rRef
            //just push one to the result list
            pList.push(lPointer);
            n=0;
        }
    }
    dbgLog('uniPickByValue, picked indices are ', JSON.stringify(pList));
    return pList;
}
