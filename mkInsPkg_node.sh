#!/bin/bash

echo "Packaging picture filter script..."
fFlush=1
subdirs=("config" "node_modules" "logs")
# subdirs=("config" "models" "routes" "utils")
files=( "fileNameParser.js" "filters.js" "imgZipFilter.js" "selectFile.js" "package.json" "package-lock.json" "logger.js")

# use customized path as source, if any
if [ $# -eq 0 ]
then
    PKG="picFilter.zip"
    echo "Use default package filename $PKG"
else
    PKG=$1
    if [ $# -eq 2 ]
    then
        fFlush=$2
    else
        fFlush=0
    fi
fi

# remove existing zip file, if any
if [ -f $PKG ]
then
    if [ $fFlush = 1 ] 
    then
        echo "Remove old package $PKG."
        rm -f $PKG
        echo "Create package file $PKG."
    else
        echo "Create/update package file $PKG."
    fi
else
    echo "Create package file $PKG."
fi

# generate the zip file of modules
for i in "${subdirs[@]}"
do
   echo "Packaging sub-folder $i..." 
   zip -ry9 $PKG $i/*
done

for i in "${files[@]}"
do
   echo "Packaging $i..." 
   zip -y9 $PKG $i
done

echo "picture filter $PKG is ready for installation."
