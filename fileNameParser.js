var path = require('path');

var parseAngle = function (aStr) {
    let sign = 1;
    if ('n' == aStr[aStr.length-1]){
        aStr = aStr.slice(0,aStr.length-1);
        sign = -1;
    }
    return sign*parseInt(aStr);
}

exports.parseFilename = function(f, lmt, fLimit) {
    let fields = {};
    do {
        if (0==lmt.formatType) {
            let tk = path.basename(f).slice(0,-4).split('-');
            if ((!Array.isArray(tk)) || (tk.length>15) || (tk.length<12)){
                // skip filtering if format seems not right
                fields = null;
                break;
            }
            fields.tstamp = tk[0]+'-'+tk[1]+'-'+tk[2];
            let tk2 = tk[2].split('_');
            fields.tstamp = tk[0]+'-'+tk[1]+'-'+tk2[0]+'-'+tk2[1]+'-'+tk2[2];
            fields.sn = tk[3];
            fields.localConf = parseInt(100*parseFloat(tk[4]));
            fields.faceSize_w = parseInt(tk[5]);
            fields.faceSize_d = parseInt(tk[6]);
            let nTk = 7;
            if (0==tk[nTk].length){
                nTk++;
                fields.angle_y = -parseInt(0.5+parseFloat(tk[nTk]));    
            }
            else {
                fields.angle_y = parseInt(0.5+parseFloat(tk[nTk]));       
            }
            nTk++;
            if (0==tk[nTk].length){
                nTk++;
                fields.angle_p = -parseInt(0.5+parseFloat(tk[nTk]));    
            }
            else {
                fields.angle_p = parseInt(0.5+parseFloat(tk[nTk]));       
            }
            nTk++;
            if (0==tk[nTk].length){
                nTk++;
                fields.angle_r = -parseInt(0.5+parseFloat(tk[nTk]));    
            }
            else {
                fields.angle_r = parseInt(0.5+parseFloat(tk[nTk]));       
            }
            nTk++;
            fields.eyeDist = parseInt(0.5+parseFloat(tk[nTk]));
            nTk++;
            fields.fid = parseInt(tk[nTk]);
            fields.ref = f;
        }
        else if(1==lmt.formatType){
            let flds = path.basename(f).split('.')[0].split('_');
            if ( (!Array.isArray(flds)) || (flds.length!=10) ){
                // skip filtering if format seems not right
                fields = null;
                break;
            }
            fields = {
                tstamp:flds[0]  // '2018-02-02T00-06-18'
                ,fid:parseInt(flds[1])    // '1825'
                ,sn:parseInt(flds[2])     // '10'
                ,angle_y:parseAngle(flds[3])    // -20
                ,angle_p:parseAngle(flds[4])    // 21
                ,angle_r:parseAngle(flds[5])    // -7
                ,eyeDist:parseInt(flds[6])      // 121
                ,faceSize_w:parseInt(flds[7])   // 260
                ,faceSize_d:parseInt(flds[8])   // 260
                ,localConf:parseInt(flds[9])    // 98
                ,ref:f
            };
        }
        else if(2==lmt.formatType){
            //2018-04-24T17-24-37_99_3_122n-240-221_61_114x114_99.png
            let flds = path.basename(f).split('.')[0].split('_');
            if ( (!Array.isArray(flds)) || (flds.length!=7) ){
                // skip filtering if format seems not right
                fields = null;
                break;
            }
            fields = {
                tstamp:flds[0]  // '2018-02-02T00-06-18'
                ,fid:parseInt(flds[1])    // '1825'
                ,sn:parseInt(flds[2])     // '10'
                ,eyeDist:parseInt(flds[4])      // 121
                ,localConf:parseInt(flds[6])    // 98
                ,ref:f
            };
            let angle = flds[3].split('-');
            fields.angle_y = parseAngle(angle[0]);
            fields.angle_p = parseAngle(angle[1]);
            fields.angle_r = parseAngle(angle[2]);

            let wd = flds[5].split('x');
            fields.faceSize_w = wd[0];
            fields.faceSize_d = wd[1];
        }
        else {
            //2018-04-24T17-24-37_99_3_122n-240-221_61_114x114_99_1157_351_1271_465.png
            let flds = path.basename(f).split('.')[0].split('_');
            if ( (!Array.isArray(flds))){
                // skip filtering if format seems not right
                fields = null;
                break;
            }
            
            
            
            if(flds.length==11){ 
                fields = {
                    tstamp:flds[0]  // '2018-02-02T00-06-18'
                    ,fid:parseInt(flds[1])    // '99'
                    ,sn:parseInt(flds[2])     // '3'
                    ,eyeDist:parseInt(flds[4])      //'61'
                    ,localConf:parseInt(flds[6])    // '99'
                    ,rectLeft:parseInt(flds[7])     //'1157'
                    ,rectTop:parseInt(flds[8])      //'351'
                    ,rectRight:parseInt(flds[9])      //'1271'
                    ,rectBottom:parseInt(flds[10])     //'465'
                    ,ref:f
                };
            }else if(flds.length==13){
                fields = {
                    tstamp:flds[0]  // '2018-02-02T00-06-18'
                    ,fid:parseInt(flds[1])    // '99'
                    ,sn:parseInt(flds[2])     // '3'
                    ,eyeDist:parseInt(flds[4])      //'61'
                    ,localConf:parseInt(flds[6])    // '99'
                    ,rectLeft:parseInt(flds[7])     //'1157'
                    ,rectTop:parseInt(flds[8])      //'351'
                    ,rectRight:parseInt(flds[9])      //'1271'
                    ,rectBottom:parseInt(flds[10])     //'465'
                    ,equId:parseInt(flds[11])
                    ,occTm:parseInt(flds[12])
                    ,ref:f
                };
            }else if(flds.length==7){
                fields = {
                    tstamp:flds[0]  // '2018-02-02T00-06-18'
                    ,fid:parseInt(flds[1])    // '1825'
                    ,sn:parseInt(flds[2])     // '10'
                    ,eyeDist:parseInt(flds[4])      // 121
                    ,localConf:parseInt(flds[6])    // 98
                    ,ref:f
                };
                let angle = flds[3].split('-');
                fields.angle_y = parseAngle(angle[0]);
                fields.angle_p = parseAngle(angle[1]);
                fields.angle_r = parseAngle(angle[2]);

                let wd = flds[5].split('x');
                fields.faceSize_w = wd[0];
                fields.faceSize_d = wd[1];
            }else{
                console.log('invalid file name format');
                break;
            }
            let angle = flds[3].split('-');
            fields.angle_y = parseAngle(angle[0]);
            fields.angle_p = parseAngle(angle[1]);
            fields.angle_r = parseAngle(angle[2]);

            let wd = flds[5].split('x');
            fields.faceSize_w = wd[0];
            fields.faceSize_d = wd[1];
        }
        if(fLimit){
            Object.keys(lmt).forEach(function(k){
                if (fields[k] <= lmt[k]){
                    fields.ref = null;
                }
            });
        }
    }while(false);
    return fields;
}