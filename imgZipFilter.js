var fs = require('fs');
var util = require('util');
var path = require('path');
const { spawn } = require('child_process');
const { spawnSync } = require('child_process');
var selectFile = require('./selectFile');
var config = require("config");
var async = require("async");
const logger = require ('./logger');

let filterLimit1 = config.get("filterCond");

const validExt = config.get("validImg");

var unpackFiles = function (arg, callback) {
    let zipDict = {};       //dictionary with zip file name as key and list of files in the zip as value
    async.eachLimit(arg.fileList, 2, function (targetPack, callback) {
        logger.debug('...unpack images from', targetPack);
        const zipls = spawn('unzip', ['-l', targetPack]);
        let fileList = [];
        zipls.stdout.on('data', (zline) => {
            let le = zline.toString().split('\n');
            le.forEach(function (l) {
                let te = l.split(' ');
                let pathToken = te[te.length - 1];
                if (validExt.includes(path.extname(pathToken))) {
                    fileList.push(pathToken);
                }
            });
        });
        zipls.on('exit', (code) => {
            if (0 != code) {
                let errMsg = util.format('failed to list zip content, errCode=%d', code);
                callback(errMsg);
                return;
            } else {
                logger.debug('Got all image info from ', targetPack);
                zipDict[targetPack] = fileList;
                callback(null);
            }
        });
    }, function (err) {
        if (err) return callback(err);
        selectFile.selectFiles(zipDict, function (err, selectedDict) {
            if (err) return callback(err);
            async.eachLimit(Object.keys(selectedDict), 2, function (zipName, callback) {
                //unzip the selected files from the zip file
                if (selectedDict[zipName].length > 0) {
                    // let unzipParams = selectedDict[zipName].concat(['-d', arg.imgPath]);
                    //let unzipParams = ['-d', arg.imgPath];
                    // const unpack = spawn('unzip', ['-uj', zipName].concat(unzipParams), { stdio: ['ignore', 'ignore', 'ignore'] });      //.concat(unzipParams)
                    // unpack.on('exit', (code) => {
                    //     if (0 == code) {
                    //         util.log("Finished zip file ", zipName);
                    //         callback(null);
                    //     } else {
                    //         let err = 'failed to decompress ' + zipName + ', errCode=' + code;
                    //         util.log(err);
                    //         callback(err);
                    //     }
                    // });
                    try {
                        logger.info(`the number of selected images(${zipName}): ` + selectedDict[zipName].length);
                        let chunkSize = 1000;
                        for (let i = 0; i < selectedDict[zipName].length / chunkSize; i++) {
                            let end = i * chunkSize + chunkSize < selectedDict[zipName].length ? i * chunkSize + chunkSize : selectedDict[zipName].length;
                            const str = selectedDict[zipName].slice(i * chunkSize, end).join(' ');
                            if (str.length > 200000) {
                                chunkSize = Math.round(chunkSize * 0.8);
                                i = 0;
                                continue;
                            }
                            const res = spawnSync('unzip',  ['-uj', zipName].concat(selectedDict[zipName].slice(i * chunkSize, end)).concat(['-d', arg.imgPath]), { stdio: ['ignore', 'ignore', 'ignore'] });
                            util.log(JSON.stringify(res));
                            if (res.status > 2) {
                                let err = 'failed to decompress ' + zipName + ', errCode is' + res.status + ',error is ' + res.error;
                                logger.error(err);
                                callback(err);
                            }
                        }
                        logger.debug("Finished unzip file ", zipName);
                        callback(null);
                    } catch (error) {
                        let err = 'failed to decompress ' + zipName + ', error is' + error;
                        logger.error(err);
                        callback(err);
                    }
                } else {
                    logger.info('No file selected from ' + zipName);
                    callback(null);
                }
            }, function (err) {
                callback(err);
            });
        });
    });
}


var getPackIndex = function (packName) {
    return parseInt(path.basename(packName).split('.')[0].split('_')[1]);
};

var compS3File = function (a, b) {
    ai = getPackIndex(a);
    bi = getPackIndex(b);

    return (ai - bi);
};

let nArg = 2;

if (process.argv.length > nArg) {
    if ('-h' === process.argv[nArg]) {
        console.log('Usage: node imgZipFilter.js <zipDir> <imgDir>');
        process.exit();
    }
}
if (process.argv.length > nArg) {
    zipDir = process.argv[nArg];
    nArg++;
}

if (process.argv.length > nArg) {
    imgDir = process.argv[nArg];
    nArg++;
}

/*
if (process.argv.length>nArg) {
    filenameType = parseInt(process.argv[nArg]);
    filterLimit1.formatType = filenameType;
    nArg++;
}

if (process.argv.length>nArg) {
    nPick = parseInt(process.argv[nArg]);
    filterLimit1.maxPick = nPick;
    nArg++;
}
*/

logger.debug('Start processing folder ', zipDir);
fs.readdir(zipDir, function (err, files) {
    if (err) {
        logger.error('faile to list folder '+zipDir+', err='+JSON.stringify(err));
        process.exit(-1);
    }
    let arg = {};
    arg.fileList = [];
    files.forEach(function (f) {
        if ('.zip' == path.extname(f)) {
            arg.fileList.push(path.join(zipDir, f));
        }
    });
    logger.info("zip files being processed are "+JSON.stringify(arg.fileList));
    //    arg.fileList = arg.fileList.sort(compS3File);
    //    arg.imgPath = path.join(zipDir,imgDir);
    arg.imgPath = imgDir;
    unpackFiles(arg, function (err) {
        if (err) {
            logger.error('faile to filter and decompress, err='+JSON.stringify(err));
            process.exit(-2);
        }
        else {
            logger.info('done.');
        }
    });
});

